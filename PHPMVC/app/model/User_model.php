<?php

class User_model extends Model 
{
    public string $table = 'User';

    public function all()
    {
        $this->db->query("SELECT * FROM {$this->table}");
        return $this->db->resultSet();
    }

    public function single(int $user_id)
    {
        $this->db->query("SELECT FROM {$this->table} WHERE user_id = :user_id");
        $this->db->bind(':user_id', $user_id);
        return $this->db->single();
    }

    public function cobaLogin(string $email, string $password)
    {
        $sql = "SELECT * FROM user WHERE email = :email";
        $passwordValid = false;

        $this->db->query($sql);
        $this->db->bind("email",$email);

        $user = $this->db->single();

        if($user){
          $passwordValid = Hash::check( $password, $user['password'] );
        }
        
        if ($passwordValid)
        {
          $_SESSION['user_login'] = [
            "nama" => $user['nama'],
            "email" => $user['email'],
            "user_id" => $user['user_id'],
            "posisi" => $user['posisi']
          ];
          return redirect("/");
        }
        
        return redirect("user/login",['fail' => "Email Atau Password Salah"]);
    }
}