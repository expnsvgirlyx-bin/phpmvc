<?php

// class User extends Controller {

//   public function index() {

//     $this->view("User/index");
//   }

//   public function profile($nama = "bintang", $pekerjaan = "BEBAN KELUARGA") {
//     $data["nama"] = $nama;
//     $data["pekerjaan"] = $pekerjaan;
//     $this->view("user/profile", $data);
//   }
// }

class User extends Controller {

  public function index($nama = "bintang", $pekerjaan = "BEBAN KELUARGA")
  {
     $data["judul"] = "User";
     $data["nama"] = $nama;
     $data["pekerjaan"] = $pekerjaan;
     $this->view("templates/header", $data);
     $this->view("user/index", $data);
     $this->view("templates/footer");
  }


  public function login()
  {
      $data["judul"] = "login";
      $this->view("user/login", $data);
  }

  public function save_login()
  {
    
    $this->model("User_model")->cobaLogin($_POST['email'], $_POST['password']);
  }
  
  public function register()
  {
      $data["judul"] = "Register";
      return $this->view("user/register", $data);
  }

  public function save_register()
  {
    $password = Hash::make($_POST['password']);
        
        $this->model('User_model')->create([
            "nama" => $_POST['nama'],
            "password" => $password,
            "noTelp" => $_POST['no_telpon'],
            "email" => $_POST['email']
        ]);

        return redirect("user/login");
  }

  public function logout()
  {
    session_unset();
    session_destroy();
    
    return redirect("user/login");
  }

}
