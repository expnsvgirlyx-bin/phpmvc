<?php

// class Home extends Controller 
// {
//   public function index() 
//   {
//       $data = [
//         "title" => 'home',
//         "nama" => 'helo'
//       ];
//       // $data['judul'] = 'Home';
//       $this->view('home/index', $data);
//   }
// }

class Home extends Controller {

  public function __construct()
    {
        if(! isset($_SESSION['user_login']))
        {
            //jik belum login maka
            return redirect("user/login");
        }
    }

  public function index() 
  {
    $data["judul"] = "Home";
    $data['nama'] = "mvcbintang";
 
    $this->view("templates/header", $data);
    $this->view("home/index", $data);
    $this->view("templates/footer");
  }

}