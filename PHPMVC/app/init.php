<?php

  require_once 'core/App.php';
  require_once 'core/Controller.php';
  require_once "config/config.php";
  require_once "core/Database.php";
  require_once "core/Model.php";
  require_once "component/function.php";
  require_once "component/Hash.php";