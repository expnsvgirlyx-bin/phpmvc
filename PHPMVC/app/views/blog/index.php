
<div class="container bg-secondary p-1 mt-4 d-flex justify-content-center" style="border-radius: 1rem;">
    <div class="d-flex bg-dark align-items-center p-1" style="width:fit-content;">
        <img class="image-fluid border"  width="auto" height="400"src="<?= BASE_URL?>/img/profile.jpg">
        </div>
    </div>
    <h3 class="text-light text-center my-3">Daftar Blog</h3>
    <div class="music-list container mb-5">
        <?php foreach($data['all-blog'] as $blog):?>
        <div class="bg-secondary shadow-lg border-top d-flex justify-content-between align-items-center shadow-md text-light px-5 py-3 mt-4 "style="border-radius: 1rem;">
            <div><?= "{$blog['judul']} - {$blog['penulis']}"?></div>
            <a href="<?= BASE_URL?>/blog/show/<?= $blog['id']?>" class="btn btn-dark">Lihat
            </a>
        </div>
        <?php endforeach ;?>
    </div>
</div>