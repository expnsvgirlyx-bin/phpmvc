<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title>Login</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet" />

    <!-- Custom styles for this template-->
    <link href="../css/style.css" rel="stylesheet" />
  </head>

  <body class="bg-dark mt-3">
    <?php if(isset($_SESSION['fail'])): ?>
      <script>alert("<?=  $_SESSION['fail']?>")</script>
    <?php endif;?>
    <div class="container">
      <!-- Outer Row -->
      <div class="row justify-content-center">
        <div class="col-xl-6 col-lg-12 col-md-9">
          <div class="card o-hidden border-0 shadow-lg-4 my-5">
            <div class="card-body bg-halfdark p-2">
              <!-- Nested Row within Card Body -->
              <div class="row">
                <div class="col-lg">
                  <div class="p-5">
                    <div class="text-center">
                      <h1 class="h4 text-white mb-4">Login</h1>
                    </div>
                    <form class="user" method="POST" action="<?= url("user/save_login")?>">
                      <div class="form-group">
                        <label for="email" class="text-white">Email</label>
                        <input name="email" type="email" class="form-control form-control-user" id="InputEmail" aria-describedby="emailHelp" placeholder="Enter Email Address..." />
                      </div>
                      <div class="form-group">
                        <label for="password" class="text-white">Password</label>
                        <input name="password" type="password" class="form-control form-control-user" id="InputPassword" placeholder="Password" />
                      </div>
                      <br>
                      <div class="form-group">
                        <div class="mt-4">
                            <button class="btn btn-secondary btn-user btn-block"> Login </button>
                        </div>
                      </div>
                      <div class="d-flex justify-content-center">
                          <p class="text-white">belum punya akun? <a href="<?= BASE_URL?>/user/register" class="text-white"> buat!</a></p>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>
  </body>
</html>
